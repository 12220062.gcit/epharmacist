// require meaning import
const express = require("express")

const app = express()
const userRouter = require("./routes/userRoutes")
app.use(express.json())
app.use("/api/v1/users", userRouter)

// exporting the app variable to use in server.js files
module.exports = app